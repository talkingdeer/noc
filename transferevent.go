package noc

import "gitlab.com/akita/akita"

type TransferEvent struct {
	*akita.EventBase
	req akita.Req
}

func NewTransferEvent(time akita.VTimeInSec, handler akita.Handler, req akita.Req) *TransferEvent {
	evt := new(TransferEvent)
	evt.EventBase = akita.NewEventBase(time, handler)
	evt.req = req
	return evt

}
