package noc

import (
	"log"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
)

func TestNOC(t *testing.T) {
	log.SetOutput(GinkgoWriter)
	RegisterFailHandler(Fail)
	RunSpecs(t, "GCN3")
}

type sendRecvAgent struct {
	akita.ComponentBase
	ticker *akita.Ticker

	out *akita.Port

	sendLeft             int
	sendCount, recvCount int
}

func (a *sendRecvAgent) Handle(e akita.Event) error {
	switch e.(type) {
	case *akita.TickEvent:
		req := akita.NewReqBase()
		req.SetSendTime(e.Time())
		req.SetSrc(a.out)
		req.SetDst(a.out)
		req.SetByteSize(64)

		err := a.out.Send(req)
		if err == nil {
			a.sendLeft--
			a.sendCount++
		}

		if a.sendLeft > 0 {
			a.ticker.TickLater(e.Time())
		}
	}
	return nil
}

func (a *sendRecvAgent) NotifyPortFree(now akita.VTimeInSec, port *akita.Port) {
}

func (a *sendRecvAgent) NotifyRecv(now akita.VTimeInSec, port *akita.Port) {
	port.Retrieve(now)
	a.recvCount++
}

func newSendRecvAgent(name string, engine akita.Engine) *sendRecvAgent {
	agent := new(sendRecvAgent)
	agent.ComponentBase = *akita.NewComponentBase(name)

	agent.ticker = akita.NewTicker(agent, engine, 1*akita.GHz)
	agent.out = akita.NewPort(agent)

	return agent
}

var _ = Describe("Fixed Bandwidth Connection", func() {
	It("Should deliver", func() {
		engine := akita.NewSerialEngine()
		conn := NewFixedBandwidthConnection(16, engine, 1*akita.MHz)
		conn.NumLanes = 1
		agent := newSendRecvAgent("Agent", engine)
		conn.PlugIn(agent.out)

		agent.sendLeft = 50
		agent.ticker.TickLater(0)

		engine.Run()

		Expect(agent.recvCount).To(Equal(agent.sendCount))
	})
})
