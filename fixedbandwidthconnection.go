package noc

import (
	"log"

	"reflect"

	"gitlab.com/akita/akita"
)

type FixedBandwidthConnection struct {
	*akita.HookableBase

	engine akita.Engine
	ticker *akita.Ticker

	BytesPerCycle int
	NumLanes      int
	busy          int

	Freq                      akita.Freq
	srcBufferCapacity         int
	dstBufferCapacity         int
	srcBuffers                map[*akita.Port][]akita.Req
	dstBuffers                map[*akita.Port][]akita.Req
	dstBusy                   map[*akita.Port]bool
	dstBuffersNumArrivingReqs map[*akita.Port]int
	needTick                  bool

	TotalDataSent uint64
}

func (c *FixedBandwidthConnection) Handle(e akita.Event) error {
	c.InvokeHook(e, c, akita.BeforeEventHookPos, nil)
	defer c.InvokeHook(e, c, akita.AfterEventHookPos, nil)

	switch evt := e.(type) {
	case *akita.TickEvent:
		c.tick(evt.Time())
	case *TransferEvent:
		c.doTransfer(evt)
	default:
		log.Panicf("cannot handle event of type %s", reflect.TypeOf(evt))
	}
	return nil
}

func (c *FixedBandwidthConnection) tick(now akita.VTimeInSec) {
	c.needTick = false

	c.doDeliver(now)
	c.scheduleTransfer(now)

	if c.needTick {
		c.ticker.TickLater(now)
	}
}

func (c *FixedBandwidthConnection) doDeliver(now akita.VTimeInSec) {
	for port, buf := range c.dstBuffers {
		if c.dstBusy[port] {
			continue
		}

		if len(buf) == 0 {
			continue
		}

		req := buf[0]
		req.SetRecvTime(now)
		err := port.Recv(req)
		if err == nil {
			buf = buf[1:]
			c.dstBuffers[port] = buf
			c.needTick = true
			c.TotalDataSent += uint64(req.ByteSize())
			c.InvokeHook(req, c, akita.ConnDeliverHookPos, nil)
		} else {
			c.dstBusy[port] = true
		}
		break
	}
}

func (c *FixedBandwidthConnection) scheduleTransfer(now akita.VTimeInSec) {
	if c.busy >= c.NumLanes {
		return
	}

	for _, buf := range c.srcBuffers {
		if len(buf) == 0 {
			continue
		}

		req := buf[0]
		dst := req.Dst()
		if len(c.dstBuffers[dst])+c.dstBuffersNumArrivingReqs[dst] >= c.dstBufferCapacity {
			continue
		}

		c.needTick = true
		cycles := ((req.ByteSize() - 1) / c.BytesPerCycle) + 1
		transferTime := c.Freq.NCyclesLater(cycles, now)
		transferEvent := NewTransferEvent(transferTime, c, req)
		c.engine.Schedule(transferEvent)
		c.InvokeHook(req, c, akita.ConnStartTransHookPos, nil)

		c.busy++
		break
	}
}

func (c *FixedBandwidthConnection) doTransfer(evt *TransferEvent) {
	now := evt.Time()
	req := evt.req
	src := req.Src()
	dst := req.Dst()

	c.srcBuffers[src] = c.srcBuffers[src][1:]
	c.dstBuffers[dst] = append(c.dstBuffers[dst], req)
	c.busy--
	c.ticker.TickLater(now)

	c.InvokeHook(req, c, akita.ConnDoneTransHookPos, nil)

	src.NotifyAvailable(evt.Time())
}

func (c *FixedBandwidthConnection) Send(req akita.Req) *akita.SendError {
	buf := c.srcBuffers[req.Src()]

	if buf == nil {
		log.Panic("not connected")
	}

	if len(buf) >= c.srcBufferCapacity {
		return akita.NewSendError()
	}

	buf = append(buf, req)
	c.srcBuffers[req.Src()] = buf
	c.ticker.TickLater(req.SendTime())

	c.InvokeHook(req, c, akita.ConnStartSendHookPos, nil)

	return nil
}

func (c *FixedBandwidthConnection) PlugIn(port *akita.Port) {
	_, connected := c.srcBuffers[port]
	if connected {
		log.Panic("port already connected")
	}

	c.srcBuffers[port] = make([]akita.Req, 0, c.srcBufferCapacity)
	c.dstBuffers[port] = make([]akita.Req, 0, c.dstBufferCapacity)
	c.dstBuffersNumArrivingReqs[port] = 0
	c.dstBusy[port] = false

	port.Conn = c
}

func (FixedBandwidthConnection) Unplug(port *akita.Port) {
	panic("implement me")
}

func (c *FixedBandwidthConnection) NotifyAvailable(now akita.VTimeInSec, port *akita.Port) {
	c.dstBusy[port] = false
	c.ticker.TickLater(now)
}

func NewFixedBandwidthConnection(
	bytesPerCycle int,
	engine akita.Engine,
	freq akita.Freq,
) *FixedBandwidthConnection {
	conn := new(FixedBandwidthConnection)
	conn.HookableBase = akita.NewHookableBase()

	conn.BytesPerCycle = bytesPerCycle
	conn.srcBufferCapacity = 1
	conn.dstBufferCapacity = 1
	conn.srcBuffers = make(map[*akita.Port][]akita.Req)
	conn.dstBuffers = make(map[*akita.Port][]akita.Req)
	conn.dstBuffersNumArrivingReqs = make(map[*akita.Port]int)
	conn.dstBusy = make(map[*akita.Port]bool)

	conn.engine = engine
	conn.Freq = freq
	conn.ticker = akita.NewTicker(conn, engine, conn.Freq)

	conn.NumLanes = 1

	return conn
}
